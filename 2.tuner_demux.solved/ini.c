/****************************************************************************
*
* Sveučilište Josipa Jurja Strossmayera u Osijeku
* Fakultet elektrotehnike, računarstva i informacijskih tehnologija
*
* -----------------------------------------------------
* Ispitni zadatak iz predmeta:
*
* DIGITALNA VIDEOTEHNIKA
* -----------------------------------------------------
* TV aplikacija
* -----------------------------------------------------
*
* \file ini.c
* \brief
* Ovaj modul se koristi za parsiranje konfiguracijske datoteke
* Kreirano on Jun 3
*
* @Author Jakob Triva
* \notes
*****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <ini.h>
#define MATCH(h, m, n) strcmp(header, h)==0 && strcmp(module, m)==0 && strcmp(name, n)==0
#define MAX_SECTION_LENGTH 30

static char* find_char(const char* s, const char* chars)
{
  while (*s && (!chars || !strchr(chars, *s))){
    s++;
  }
  return (char*)s;
}

int parse(configuration *cfg, const char* header, const char* module, const char* name, const char* value)
{
    if (MATCH("initial_config", "transponder", "frequency")) {
        cfg->frequency = atoi(value);
    } else if (MATCH("initial_config", "transponder", "module")) {
        cfg->module = atoi(value);
    } else if (MATCH("initial_config", "transponder", "bandwidth")) {
        cfg->bandwidth = atoi(value);
    } else if (MATCH("initial_config", "initial_channel", "apid")) {
        cfg->apid = atoi(value);
    } else if (MATCH("initial_config", "initial_channel", "atype")) {
        cfg->atype = atoi(value);
    } else if (MATCH("initial_config", "initial_channel", "vpid")) {
        cfg->vpid = atoi(value);
    } else if (MATCH("initial_config", "initial_channel", "vtype")) {
        cfg->vtype = atoi(value);
    } else {
        return 0;
    }
    return 1;
}

void CFG_PrintConfig(configuration *cfg){
  printf("frequency=%d\n", cfg->frequency);
  printf("module=%d\n", cfg->module);
  printf("bandwidth=%d\n", cfg->bandwidth);
  printf("apid=%d\n", cfg->apid);
  printf("atype=%d\n", cfg->atype);
  printf("vpid=%d\n", cfg->vpid);
  printf("vtype=%d\n", cfg->vtype);
}

int CFG_ReadConfig(configuration *cfg, char* fileName){
  FILE *fp;
  fp=fopen(fileName, "r");
  char header[MAX_SECTION_LENGTH]="";
  char module[MAX_SECTION_LENGTH]="";
  char name[MAX_SECTION_LENGTH]="";
  char value[MAX_SECTION_LENGTH]="";
  char* start=NULL;
  char* end;
  end = (char*)malloc(sizeof(char));
  int size;
  size_t len=0;
  while((getline(&start, &len, fp)) != -1){
    while (isspace(*start))
      ++start;

    if(start[0]=='['){
      end=find_char(start+1, "]=");
      if(*end==']'){
        *end='\0';
        size= end-start;
        if(header[0]== '\0'){
          strncpy(header, start+1, sizeof(value));
        }
        else{
          strncpy(module, start+1, sizeof(value));
        }
      }
      else if(*end=='='){
        *end='\0';
        size= end-start;
        strncpy(name, start+1, sizeof(value));
        start=end+1;
        end=find_char(start, "]");
        if(*end==']'){
          *end='\0';
          size= end-start;
          strncpy(value, start, sizeof(value));
        }
        if(!parse(cfg, header, module, name, value)){
          return 0;
        }
      }
    }
  }
  fclose(fp);
  return 1;
}
