/****************************************************************************
*
* Sveučilište Josipa Jurja Strossmayera u Osijeku
* Fakultet elektrotehnike, računarstva i informacijskih tehnologija
*
* -----------------------------------------------------
* Ispitni zadatak iz predmeta:
*
* DIGITALNA VIDEOTEHNIKA
* -----------------------------------------------------
* TV aplikacija
* -----------------------------------------------------
*
* \file main.c
* \brief
* Glavni modul
* Kreirano on Jun 3
*
* @Author Jakob Triva
* \notes
*****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

#include "remote.h"
#include "graphic.h"
#include "demux.h"

int32_t main(int32_t argc, char** argv)
{
  timerParameters timerParams;
  graphicParameters graphicParams;
  remoteParameters remoteParams;
  demuxParameters demuxParams;


  configuration cfg;
  uint16_t apid = 0;
  uint16_t vpid = 0;
  uint16_t i=0;
  GP_Init(&graphicParams, argc, argv);
  TP_Init(&timerParams, &graphicParams);

  if(!CFG_ReadConfig(&cfg, argv[1]))
  {
    printf("Error reading configuration file\n");
  }
  else
  {
    printf("Success reading configuration file\n");
  }

  DM_Init(&demuxParams, &cfg);

  demuxParams.result = Player_Stream_Create(demuxParams.playerHandle, demuxParams.sourceHandle, cfg.vpid, cfg.vtype, &demuxParams.videoHandle);
  demuxParams.result = Player_Stream_Create(demuxParams.playerHandle, demuxParams.sourceHandle, cfg.apid, cfg.atype, &demuxParams.audioHandle);

  DM_ParsePat(&demuxParams);
  DM_ParsePmt(&demuxParams);
  DM_ParseEit(&demuxParams);

  RM_Init(&remoteParams);
  fflush(stdin);

  while(!remoteParams.powerOff)
  {
    if(remoteParams.infoFlag)
    {
      remoteParams.infoFlag=0;
      GP_drawInfo(remoteParams.currentCh, &graphicParams, &timerParams, PMT_FindSubtitles(&pmts[remoteParams.currentCh-1]));
    }
    if(remoteParams.channelChanged)
    {
      remoteParams.channelChanged=0;
      graphicParams.chFlag=1;
      GP_drawInfo(remoteParams.dialedCh, &graphicParams, &timerParams, PMT_FindSubtitles(&pmts[remoteParams.currentCh-1]));
    }
    if(graphicParams.chFlag==2)
    {
      graphicParams.chFlag=0;
      if(remoteParams.dialedCh<remoteParams.nOfChannels && remoteParams.dialedCh!=remoteParams.currentCh)
      {
        GP_paintBlack(&graphicParams);
        apid = PMT_FindApid(&pmts[remoteParams.currentCh-1]);
        vpid = PMT_FindVpid(&pmts[remoteParams.currentCh-1]);
        //GP_paintBlack(&graphicParams);
        if(vpid)
        {
          Player_Stream_Remove(demuxParams.playerHandle, demuxParams.sourceHandle, demuxParams.videoHandle);
        }
        if(apid)
        {
          Player_Stream_Remove(demuxParams.playerHandle, demuxParams.sourceHandle, demuxParams.audioHandle);
        }

        remoteParams.currentCh=remoteParams.dialedCh;
        apid = PMT_FindApid(&pmts[remoteParams.currentCh-1]);
        vpid = PMT_FindVpid(&pmts[remoteParams.currentCh-1]);
        if(vpid)
        {
          remoteParams.menuCh=remoteParams.currentCh;
          demuxParams.result = Player_Stream_Create(demuxParams.playerHandle, demuxParams.sourceHandle, vpid, VIDEO_TYPE_MPEG2, &demuxParams.videoHandle);
          GP_clearScreen(&graphicParams);
        }
        else
        {
          GP_paintBlack(&graphicParams);
        }
        if(apid)
        {
          demuxParams.result = Player_Stream_Create(demuxParams.playerHandle, demuxParams.sourceHandle, apid, AUDIO_TYPE_MPEG_AUDIO, &demuxParams.audioHandle);
        }
        DM_ParseEit(&demuxParams);

      }
      else
      {
        GP_drawInfo(remoteParams.currentCh, &graphicParams, &timerParams, PMT_FindSubtitles(&pmts[remoteParams.currentCh-1]));
      }
      remoteParams.dialedCh=0;
    }
    if(remoteParams.volumeChanged)
    {
      if(remoteParams.mute)
      {
        GP_drawVolume(0, &graphicParams, &timerParams);
      	Player_Volume_Set(demuxParams.playerHandle, 0);
      }
      else{
        GP_drawVolume(remoteParams.currentVol, &graphicParams, &timerParams);
      	Player_Volume_Set(demuxParams.playerHandle, (remoteParams.currentVol/100.0)*INT32_MAX);
      }
      remoteParams.volumeChanged=0;
    }
    if(remoteParams.menuFlag)
    {
      if(remoteParams.menuChFlag){
        GP_drawMenu(&graphicParams, &timerParams, eits[remoteParams.menuCh-1].currentDescription, eits[remoteParams.menuCh-1].nextDescription, remoteParams.menuCh);
        remoteParams.menuChFlag=0;
      }
      if(remoteParams.menuFlag==2)
      {
        GP_clearScreen(&graphicParams);
        remoteParams.menuFlag=0;
      }
    }
    else if(remoteParams.menuChFlag)
    {
      remoteParams.menuChFlag=0;
    }
    fflush(stdin);
  }

  RM_Denit(&remoteParams);
  GP_Deinit(&timerParams, &graphicParams);
  DM_Deinit(&demuxParams);
  return 0;
}