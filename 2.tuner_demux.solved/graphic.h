/****************************************************************************
*
* Sveučilište Josipa Jurja Strossmayera u Osijeku
* Fakultet elektrotehnike, računarstva i informacijskih tehnologija
*
* -----------------------------------------------------
* Ispitni zadatak iz predmeta:
*
* DIGITALNA VIDEOTEHNIKA
* -----------------------------------------------------
* TV aplikacija
* -----------------------------------------------------
*
* \file demux.h
* \brief
* Ovaj modul se koristi za rad grafike
* Kreirano on Jun 3
*
* @Author Jakob Triva
* \notes
*****************************************************************************/
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <directfb.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>


typedef struct
{
	IDirectFBSurface *primary;
	IDirectFB *dfbInterface;
	int screenWidth;
	int screenHeight;
	DFBSurfaceDescription surfaceDesc;
	IDirectFBFont *fontInterface;
	DFBFontDescription fontDesc;
  uint8_t chFlag;
}graphicParameters;

typedef struct
{
	timer_t timerId;
	int32_t timerFlags;
	struct itimerspec timerSpec;
	struct itimerspec timerSpecOld;
	struct sigevent signalEvent;
}timerParameters;

/***************************************************************************
*
* @brief Inicijalizacija parametara za grafičko sučelje.
*
* @param graphicParams- struktura u koje ce se zapisati inicijalizacijski parametri
*
****************************************************************************/
void GP_Init(graphicParameters *graphicParams, int32_t argc, char** argv);
/***************************************************************************
*
* @brief Inicijalizacija parametara za timere
*
* @param graphicParams- struktura u koje ce se zapisati inicijalizacijski parametri
*
****************************************************************************/
void TP_Init(timerParameters *timerParams, graphicParameters *graphicParams);
/***************************************************************************
*
* @brief Crtanje info dijaloga
*
* @param currentCh- broj trenutnog kanala
* @param graphicParams- struktura u kojoj su pohranjene vrijednosti potrebne za grafiku
* @param timerParams- struktura u kojoj su pohranjene vrijednosti potrebne za timere
* @param subtitles- bool, pokazuje jesu li prisutni podnaslovi
*
****************************************************************************/
void GP_drawInfo(uint16_t currentCh, graphicParameters *graphicParams, timerParameters *timerParams, int subtitles);
/***************************************************************************
*
* @brief Crtanje menu dijaloga
*
* @param graphicParams- struktura u kojoj su pohranjene vrijednosti potrebne za grafiku
* @param timerParams- struktura u kojoj su pohranjene vrijednosti potrebne za timere
* @param current- tekst trenutne emisije
* @param next- text sljedeće emisije
* @param currentCh- broj trenutnog kanala
*
****************************************************************************/
void GP_drawMenu(graphicParameters *graphicParams, timerParameters *timerParams, char *current, char *next, uint16_t currentCh);
/***************************************************************************
*
* @brief Crtanje jačine zvuka
*
* @param volume- jačina zvuka
* @param graphicParams- struktura u kojoj su pohranjene vrijednosti potrebne za grafiku
* @param timerParams- struktura u kojoj su pohranjene vrijednosti potrebne za timere
*
****************************************************************************/
void GP_drawVolume(int volume, graphicParameters *graphicParams, timerParameters *timerParams);
/***************************************************************************
*
* @brief Timer funkcija
*
****************************************************************************/
void timerFunction(union sigval sv);
/***************************************************************************
*
* @brief Denicijalizacija parametara za grafičko sučelje.
*
* @param graphicParams- struktura u kojoj su zapisani parametri grafike
* @param timerParams- struktura u kojoj su zapisani parametri timera
*
****************************************************************************/
void GP_Deinit(timerParameters *timerParams, graphicParameters *graphicParams);
/***************************************************************************
*
* @brief Funkcija za zacrnjenje ekrana
*
* @param graphicParams- struktura u kojoj su zapisani parametri grafike
*
****************************************************************************/
void GP_paintBlack(graphicParameters *graphicParams);
/***************************************************************************
*
* @brief Funkcija za brisanje ekrana
*
* @param graphicParams- struktura u kojoj su zapisani parametri grafike
*
****************************************************************************/
void GP_clearScreen(graphicParameters *graphicParams);
