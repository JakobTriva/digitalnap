/****************************************************************************
*
* Sveučilište Josipa Jurja Strossmayera u Osijeku
* Fakultet elektrotehnike, računarstva i informacijskih tehnologija
*
* -----------------------------------------------------
* Ispitni zadatak iz predmeta:
*
* DIGITALNA VIDEOTEHNIKA
* -----------------------------------------------------
* TV aplikacija
* -----------------------------------------------------
*
* \file remote.c
* \brief
* Ovaj modul se koristi za rad daljinksog upravljača
* Kreirano on Jun 3
*
* @Author Jakob Triva
* \notes
*****************************************************************************/
#include "remote.h"

int RM_Init(remoteParameters *remoteParams, PAT_table *pat, PMT_table *pmts)
{
  remoteParams->currentVol=50;
  const char* dev = "/dev/input/event0";
  remoteParams->eventBuf = malloc(NUM_EVENTS * sizeof(struct input_event));
  remoteParams->inputFileDesc = open(dev, O_RDWR);
  remoteParams->powerOff=0;
  remoteParams->mute=0;
  remoteParams->volumeChanged=0;
  remoteParams->channelChanged=0;
  remoteParams->dialedCh=0;
  remoteParams->menuFlag=0;
  remoteParams->infoFlag=0;
  if(remoteParams->inputFileDesc == -1)
  {
    printf("Error while opening device (%s) !", strerror(errno));
    return ERROR;
  }

  ioctl(remoteParams->inputFileDesc, EVIOCGNAME(sizeof(remoteParams->deviceName)), remoteParams->deviceName);
  printf("RC device opened succesfully [%s]\n", remoteParams->deviceName);

  remoteParams->nOfChannels=pat->nOfChannels;

  int i;
  for(i=1;i<pat->nOfChannels;i++)
  {
    if(pmts[i-1]->elemPids[0]==cfg->vpid)
    {
      remoteParams->currentCh=i;
    }
  }
  pthread_create(remoteParams->remoteThreadId, NULL, RM_readRemote, remoteParams);

  return NO_ERROR;
}

void RM_Denit(remoteParameters *remoteParams)
{
  pthread_join(remoteParams->remoteThreadId, NULL);
  free(remoteParams->eventBuf);
}

int32_t RM_getKeys(int32_t count, remoteParameters *remoteParams)
{
    int32_t ret = 0;
    /* read input events and put them in buffer */
    ret = read(remoteParams->inputFileDesc, (uint8_t*)remoteParams->eventBuf, (size_t)(count * (int)sizeof(struct input_event)));
    if(ret <= 0)
    {
        printf("Error code %d", ret);
        return ERROR;
    }
    /* calculate number of read events */
    remoteParams->eventCnt = ret / (int)sizeof(struct input_event);
    return NO_ERROR;
}

void *RM_readRemote(void *args)
{
  remoteParameters *remoteParams = args;
  uint32_t i;
  if(!remoteParams->eventBuf)
  {
    printf("Error allocating memory !");
    return NULL;
  }
  while(!remoteParams->powerOff)
  {
    if(RM_getKeys(NUM_EVENTS, remoteParams ))
    {
    	printf("Error while reading input events !");
    	return NULL;
    }

    for(i = 0; i < remoteParams->eventCnt; i++)
    {
    	if(remoteParams->eventBuf[i].type==1 && remoteParams->eventBuf[i].value==1)
      {
    		switch(remoteParams->eventBuf[i].code){
          case 2:
            remoteParams->dialedCh=remoteParams->dialedCh*10+1;
            remoteParams->channelChanged=1;
            break;
          case 3:
            remoteParams->dialedCh=remoteParams->dialedCh*10+2;
            remoteParams->channelChanged=1;
            break;
          case 4:
            remoteParams->dialedCh=remoteParams->dialedCh*10+3;
            remoteParams->channelChanged=1;
            break;
          case 5:
            remoteParams->dialedCh=remoteParams->dialedCh*10+4;
            remoteParams->channelChanged=1;
            break;
          case 6:
            remoteParams->dialedCh=remoteParams->dialedCh*10+5;
            remoteParams->channelChanged=1;
            break;
          case 7:
            remoteParams->dialedCh=remoteParams->dialedCh*10+6;
            remoteParams->channelChanged=1;
            break;
          case 8:
            remoteParams->dialedCh=remoteParams->dialedCh*10+7;
            remoteParams->channelChanged=1;
            break;
          case 9:
            remoteParams->dialedCh=remoteParams->dialedCh*10+8;
            remoteParams->channelChanged=1;
            break;
          case 10:
            remoteParams->dialedCh=remoteParams->dialedCh*10+9;
            remoteParams->channelChanged=1;
            break;
          case 11:
            remoteParams->dialedCh=remoteParams->dialedCh*10;
            remoteParams->channelChanged=1;
            break;
    			case 61:
            if(remoteParams->dialedCh==0)
            {
              remoteParams->dialedCh=remoteParams->currentCh;
            }
    				if(remoteParams->dialedCh>1)
            {
              remoteParams->channelChanged=1;
              remoteParams->dialedCh--;
            }
    				break;
    			case 62:
            if(remoteParams->dialedCh==0){
              remoteParams->dialedCh=remoteParams->currentCh;
            }
    				remoteParams->dialedCh++;
            remoteParams->channelChanged=1;
    				break;
          case 60:
            if(remoteParams->mute){
              remoteParams->mute=0;
            }
            else{
              remoteParams->mute=1;
            }
            remoteParams->volumeChanged=1;
            break;
    			case 63:
    				if(remoteParams->currentVol<MAX_VOLUME)
    					remoteParams->currentVol++;
            remoteParams->volumeChanged=1;
    				break;
    			case 64:
    				if(remoteParams->currentVol>0)
    					remoteParams->currentVol--;
            remoteParams->volumeChanged=1;
    				break;
          case 105:
            if(remoteParams->menuCh>1)
            {
              remoteParams->menuCh-=1;
            }
            remoteParams->menuChFlag=1;
            break;
          case 106:
            if(remoteParams->menuCh<4)
            {
              remoteParams->menuCh+=1;
            }
            remoteParams->menuChFlag=1;
            break;
          case 116:
            printf("\nThe end\n");
            remoteParams->powerOff=1;
            break;
          case 358:
            remoteParams->infoFlag=1;
            break;
          case 369:
            if(remoteParams->menuFlag)
            {
              remoteParams->menuFlag=2;
            }
            else{
              remoteParams->menuFlag=1;
              remoteParams->menuChFlag=1;
            }
            break;
  		  }
        if(remoteParams->dialedCh>1000)
        {
          remoteParams->dialedCh=999;
        }
      }
    }
  }
  return NULL;
}
