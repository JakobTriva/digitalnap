/****************************************************************************
*
* Sveučilište Josipa Jurja Strossmayera u Osijeku
* Fakultet elektrotehnike, računarstva i informacijskih tehnologija
*
* -----------------------------------------------------
* Ispitni zadatak iz predmeta:
*
* DIGITALNA VIDEOTEHNIKA
* -----------------------------------------------------
* TV aplikacija
* -----------------------------------------------------
*
* \file graphic.c
* \brief
* Ovaj modul se koristi za rad grafike
* Kreirano on Jun 3
*
* @Author Jakob Triva
* \notes
*****************************************************************************/
#include "graphic.h"

#define DFBCHECK(x...)                                          \
{                                                               \
DFBResult err = x;                                              \
                                                                \
if (err != DFB_OK)                                              \
  {                                                             \
    fprintf( stderr, "%s <%d>:\n\t", __FILE__, __LINE__ );      \
    DirectFBErrorFatal( #x, err );                              \
  }                                                             \
}

void TP_Init(timerParameters *str, graphicParameters *graphicParams)
{
  str->signalEvent.sigev_notify_function = &timerFunction;
  str->signalEvent.sigev_notify = SIGEV_THREAD;
  str->signalEvent.sigev_value.sival_ptr = graphicParams;
  str->signalEvent.sigev_notify_attributes = NULL;
  timer_create(/*sistemski sat za merenje vremena*/ CLOCK_REALTIME,/*podešavanja timer-a*/ &str->signalEvent,/*mesto gde će se smestiti ID novog timer-a*/ &str->timerId);
  memset(&str->timerSpec,0,sizeof(str->timerSpec));
  str->timerSpec.it_value.tv_sec = 4; //3 seconds timeout
  str->timerSpec.it_value.tv_nsec = 0;
}

void GP_Init(graphicParameters *graphicParams, int32_t argc, char** argv)
{
  graphicParams->primary = NULL;
  graphicParams->dfbInterface = NULL;
  graphicParams->screenWidth = 0;
  graphicParams->screenHeight = 0;
  graphicParams->chFlag=0;
  graphicParams->fontInterface = NULL;
  DFBCHECK(DirectFBInit(&argc, &argv));
  DFBCHECK(DirectFBCreate(&graphicParams->dfbInterface));
  DFBCHECK(graphicParams->dfbInterface->SetCooperativeLevel(graphicParams->dfbInterface, DFSCL_FULLSCREEN));
  graphicParams->surfaceDesc.flags = DSDESC_CAPS;
  graphicParams->surfaceDesc.caps = DSCAPS_PRIMARY | DSCAPS_FLIPPING;
  DFBCHECK (graphicParams->dfbInterface->CreateSurface(graphicParams->dfbInterface, &graphicParams->surfaceDesc, &graphicParams->primary));
  DFBCHECK (graphicParams->primary->GetSize(graphicParams->primary, &graphicParams->screenWidth, &graphicParams->screenHeight));
  graphicParams->fontDesc.flags = DFDESC_HEIGHT;
  graphicParams->fontDesc.height = 48;
  DFBCHECK(graphicParams->dfbInterface->CreateFont(graphicParams->dfbInterface, "/home/galois/fonts/DejaVuSans.ttf", &graphicParams->fontDesc, &graphicParams->fontInterface));
  DFBCHECK(graphicParams->primary->SetFont(graphicParams->primary, graphicParams->fontInterface));
  DFBCHECK(graphicParams->primary->SetEncoding(graphicParams->primary, DTEID_UTF8));
}

void GP_Deinit(timerParameters *timerParams, graphicParameters *graphicParams)
{
  memset(&timerParams->timerSpec,0,sizeof(timerParams->timerSpec));
  timer_settime(timerParams->timerId,0,&timerParams->timerSpec,&timerParams->timerSpecOld);
  graphicParams->primary->Release(graphicParams->primary);
  graphicParams->dfbInterface->Release(graphicParams->dfbInterface);
  timer_delete(timerParams->timerId);
}
void GP_paintBlack(graphicParameters *graphicParams)
{
  DFBCHECK(graphicParams->primary->SetColor(/*surface to draw on*/ graphicParams->primary,/*red*/ 0x00,/*green*/ 0x00,/*blue*/ 0x00,/*alpha*/ 0xff));
  DFBCHECK(graphicParams->primary->FillRectangle(/*surface to draw on*/ graphicParams->primary,/*upper left x coordinate*/ 0,/*upper left y coordinate*/ 0,/*rectangle width*/ graphicParams->screenWidth,/*rectangle height*/ graphicParams->screenHeight));
  DFBCHECK(graphicParams->primary->Flip(graphicParams->primary,/*region to be updated, NULL for the whole surface*/NULL,/*flip flags*/0));
}

void GP_clearScreen(graphicParameters *graphicParams)
{
  DFBCHECK(graphicParams->primary->SetColor(/*surface to draw on*/ graphicParams->primary,/*red*/ 0x00,/*green*/ 0x00,/*blue*/ 0x00,/*alpha*/ 0x00));
  DFBCHECK(graphicParams->primary->FillRectangle(/*surface to draw on*/ graphicParams->primary,/*upper left x coordinate*/ 0,/*upper left y coordinate*/ 0,/*rectangle width*/ graphicParams->screenWidth,/*rectangle height*/ graphicParams->screenHeight));
  DFBCHECK(graphicParams->primary->Flip(graphicParams->primary,/*region to be updated, NULL for the whole surface*/NULL,/*flip flags*/0));
}

void timerFunction(union sigval sv)
{
    graphicParameters *graphicParams = sv.sival_ptr;
	 	DFBCHECK(graphicParams->primary->SetColor(/*surface to draw on*/ graphicParams->primary,/*red*/ 0x00,/*green*/ 0x00,/*blue*/ 0x00,/*alpha*/ 0x00));
		DFBCHECK(graphicParams->primary->FillRectangle(/*surface to draw on*/ graphicParams->primary,/*upper left x coordinate*/ 0,/*upper left y coordinate*/ 0,/*rectangle width*/ graphicParams->screenWidth,/*rectangle height*/ graphicParams->screenHeight));
		DFBCHECK(graphicParams->primary->Flip(graphicParams->primary,/*region to be updated, NULL for the whole surface*/NULL,/*flip flags*/0));
    //printf("%d\n", graphicParams->chFlag);
    if(graphicParams->chFlag==1){
      graphicParams->chFlag=2;
    }
}

void GP_drawInfo(uint16_t channel, graphicParameters *graphicParams, timerParameters *timerParams, int subtitles)
{
  int x0=0;
  int y0=0;
  int a=0.26*graphicParams->screenWidth;
  int b=0.16*graphicParams->screenHeight;

  char channelString[12];
  sprintf(channelString, "Channel %d", channel);
  DFBCHECK(graphicParams->primary->SetColor( graphicParams->primary, 0x00, 0x00, 0x00, 0x00));
  DFBCHECK(graphicParams->primary->FillRectangle( graphicParams->primary, x0, y0, graphicParams->screenWidth, graphicParams->screenHeight));

  DFBCHECK(graphicParams->primary->SetColor( graphicParams->primary, 0x00, 0x00, 0x00, 0xfe));
  DFBCHECK(graphicParams->primary->FillRectangle( graphicParams->primary, x0, y0, a, b));

  a=a/1.7;
  b=b/0.6;
  DFBCHECK(graphicParams->primary->SetColor(graphicParams->primary, 0xff, 0xff, 0xff, 0xff));
  DFBCHECK(graphicParams->primary->DrawString(graphicParams->primary, channelString, -1, x0+0.1*a, y0+0.35*b, DSTF_LEFT));
  if(subtitles)
  {
    DFBCHECK(graphicParams->primary->DrawRectangle(graphicParams->primary, x0+1.1*a, y0+0.2*b, 0.3*a, 0.2*b));
  }
  DFBCHECK(graphicParams->primary->Flip(graphicParams->primary,/*region to be updated, NULL for the whole surface*/NULL,/*flip flags*/0));

  timer_settime(timerParams->timerId, timerParams->timerFlags, &timerParams->timerSpec, &timerParams->timerSpecOld);
}

void GP_drawMenu(graphicParameters *graphicParams, timerParameters *timerParams, char *current, char *next, uint16_t currentCh)
{
  char channelString[12];
  sprintf(channelString, "Channel %d:", currentCh);

  int x0, y0, a, b;
  x0=(0.35/8.5)*graphicParams->screenWidth;
  y0=(0.6/4.9)*graphicParams->screenHeight;
  a=(7.8/8.5)*graphicParams->screenWidth;
  b=(4/4.9)*graphicParams->screenHeight;
  DFBCHECK(graphicParams->primary->SetColor(/*surface to draw on*/ graphicParams->primary,/*red*/ 0x00,/*green*/ 0x00,/*blue*/ 0x00,/*alpha*/ 0x00));
  DFBCHECK(graphicParams->primary->FillRectangle(/*surface to draw on*/ graphicParams->primary,/*upper left x coordinate*/ 0,/*upper left y coordinate*/ 0,/*rectangle width*/ graphicParams->screenWidth,/*rectangle height*/ graphicParams->screenHeight));

  DFBCHECK(graphicParams->primary->SetColor(/*surface to draw on*/ graphicParams->primary,/*red*/ 0x00,/*green*/ 0x00,/*blue*/ 0x00,/*alpha*/ 0xa8));
  DFBCHECK(graphicParams->primary->FillRectangle(/*surface to draw on*/ graphicParams->primary,/*upper left x coordinate*/ x0,/*upper left y coordinate*/ y0,/*rectangle width*/ a,/*rectangle height*/ b));

  a=a/7.8;
  b=b/4;
  DFBCHECK(graphicParams->primary->SetColor(graphicParams->primary, 0xff, 0xff, 0xff, 0xff));
  DFBCHECK(graphicParams->primary->FillTriangle(graphicParams->primary, x0 + 0.1*a, y0 + 2.3*b, x0 + 0.4*a, y0 + 2.1*b, x0 + 0.4*a, y0+2.5*b));
  DFBCHECK(graphicParams->primary->FillTriangle(graphicParams->primary, x0 + 7.7*a, y0 + 2.3*b, x0 + 7.4*a, y0 + 2.1*b, x0 + 7.4*a, y0+2.5*b));

  DFBCHECK(graphicParams->primary->FillRectangle(graphicParams->primary, x0 + 0.5*a, y0 + 0.6*b, 0.05*a, 1.45*b));
  DFBCHECK(graphicParams->primary->FillRectangle(graphicParams->primary, x0 + 0.5*a, y0 + 0.6*b, 6.75*a, 0.05*b));
  DFBCHECK(graphicParams->primary->FillRectangle(graphicParams->primary, x0 + 0.5*a, y0 + 2*b, 6.75*a, 0.05*b));
  DFBCHECK(graphicParams->primary->FillRectangle(graphicParams->primary, x0 + 7.2*a, y0 + 0.6*b, 0.05*a, 1.45*b));

  DFBCHECK(graphicParams->primary->FillRectangle(graphicParams->primary, x0 + 0.5*a, y0 + 2.4*b, 0.05*a, 1.45*b));
  DFBCHECK(graphicParams->primary->FillRectangle(graphicParams->primary, x0 + 0.5*a, y0 + 2.4*b, 6.75*a, 0.05*b));
  DFBCHECK(graphicParams->primary->FillRectangle(graphicParams->primary, x0 + 0.5*a, y0 + 3.8*b, 6.75*a, 0.05*b));
  DFBCHECK(graphicParams->primary->FillRectangle(graphicParams->primary, x0 + 7.2*a, y0 + 2.4*b, 0.05*a, 1.45*b));


  DFBCHECK(graphicParams->primary->SetColor(graphicParams->primary, 0xff, 0xff, 0xff, 0xff));
  DFBCHECK(graphicParams->primary->DrawString(graphicParams->primary,channelString,-1, x0 + 0.6*a, y0 + 0.4*b,DSTF_LEFT));

  DFBCHECK(graphicParams->primary->DrawString(graphicParams->primary,"Now:",-1, x0 + 0.6*a, y0 + 0.9*b,DSTF_LEFT));

  uint16_t nOfChars=strlen(current);
  int stringWidth;
  DFBCHECK(graphicParams->fontInterface->GetStringWidth(graphicParams->fontInterface,current, nOfChars, &stringWidth));
  uint16_t rowWidth=7.3*a-0.6*a;
  uint16_t nOfRows=1+((stringWidth-1)/rowWidth);
  uint16_t nOfRowChars=nOfChars/nOfRows;
  uint16_t i;
  uint16_t j;
  char *substr;
  substr=(char*)malloc((nOfRowChars+1)*sizeof(char));

  if(nOfRows>3)
  {
    nOfRows=3;
  }
  for(i=0;i<nOfRows;i++)
  {
    strncpy(substr, current+nOfRowChars*i, nOfRowChars);
    DFBCHECK(graphicParams->primary->DrawString(graphicParams->primary,substr,-1, x0 + 0.6*a, y0 + 1.2*b + 0.3*i*b,DSTF_LEFT));
  }

  DFBCHECK(graphicParams->primary->DrawString(graphicParams->primary,"Next:",-1, x0 + 0.6*a, y0 + 2.7*b,DSTF_LEFT));
  nOfChars=strlen(next);
  DFBCHECK(graphicParams->fontInterface->GetStringWidth(graphicParams->fontInterface,next, nOfChars, &stringWidth));
  nOfRows=1+((stringWidth-1)/rowWidth);
  nOfRowChars=nOfChars/nOfRows;
  substr=(char*)malloc((nOfRowChars+1)*sizeof(char));

  if(nOfRows>3)
  {
    nOfRows=3;
  }
  for(i=0;i<nOfRows;i++)
  {
    strncpy(substr, next+nOfRowChars*i, nOfRowChars);
    DFBCHECK(graphicParams->primary->DrawString(graphicParams->primary,substr,-1, x0 + 0.6*a, y0 + 3*b + 0.3*i*b,DSTF_LEFT));
  }
  DFBCHECK(graphicParams->primary->Flip(graphicParams->primary,NULL,0));
}

void GP_drawVolume(int volume, graphicParameters *graphicParams, timerParameters *timerParams)
{
  int x0, y0, a, b, xVolume, yVolume;

  x0 = graphicParams->screenWidth*0.75;
  y0 = 0;
  a = (graphicParams->screenWidth*0.25)/1.7;
  b = (graphicParams->screenHeight*0.17)/0.6;
  xVolume = x0+a*((volume/100.0)+0.5);
  yVolume = (-0.3*b/a)*(xVolume-(x0 + 1.5*a))+ (y0 + 0.1*b);

  DFBCHECK(graphicParams->primary->SetColor(/*surface to draw on*/ graphicParams->primary,/*red*/ 0x00,/*green*/ 0x00,/*blue*/ 0x00,/*alpha*/ 0x00));
  DFBCHECK(graphicParams->primary->FillRectangle(/*surface to draw on*/ graphicParams->primary,/*upper left x coordinate*/ 0,/*upper left y coordinate*/ 0,/*rectangle width*/ graphicParams->screenWidth,/*rectangle height*/ graphicParams->screenHeight));

  DFBCHECK(graphicParams->primary->SetColor(graphicParams->primary,0x00, 0x00, 0x00, 0xee));
  DFBCHECK(graphicParams->primary->FillRectangle(graphicParams->primary, x0, y0, graphicParams->screenWidth, b*0.6));

  DFBCHECK(graphicParams->primary->SetColor(graphicParams->primary, 0xff, 0xff, 0xff, 0xef));

  DFBCHECK(graphicParams->primary->DrawLine(graphicParams->primary, x0 + 0.5*a, y0 + 0.4*b, x0 + 1.5*a, y0 + 0.4*b));
  DFBCHECK(graphicParams->primary->DrawLine(graphicParams->primary, x0 + 1.5*a, y0 + 0.4*b, x0 + 1.5*a, y0 + 0.1*b));
  DFBCHECK(graphicParams->primary->DrawLine(graphicParams->primary, x0 + 0.5*a, y0 + 0.4*b, x0 + 1.5*a, y0 + 0.1*b));
  DFBCHECK(graphicParams->primary->FillTriangle(graphicParams->primary, x0 + 0.5*a, y0 + 0.4*b, xVolume, y0 + 0.4*b, xVolume, yVolume));

  DFBCHECK(graphicParams->primary->FillRectangle(graphicParams->primary, x0 + 0.1*a, y0 + 0.3*b, 0.2*a , 0.11*b));
  DFBCHECK(graphicParams->primary->FillTriangle(graphicParams->primary, x0 + 0.2*a, y0 + 0.3*b, x0 + 0.3*a, y0 + 0.3*b, x0 + 0.3*a, y0+0.2*b));
  DFBCHECK(graphicParams->primary->FillTriangle(graphicParams->primary, x0 + 0.2*a, y0 + 0.4*b, x0 + 0.3*a, y0 + 0.4*b, x0 + 0.3*a, y0+0.5*b));

  DFBCHECK(graphicParams->primary->Flip(graphicParams->primary,/*region to be updated, NULL for the whole surface*/NULL,/*flip flags*/0));
  timer_settime(timerParams->timerId, timerParams->timerFlags, &timerParams->timerSpec, &timerParams->timerSpecOld);
}
