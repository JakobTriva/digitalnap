/****************************************************************************
*
* Sveučilište Josipa Jurja Strossmayera u Osijeku
* Fakultet elektrotehnike, računarstva i informacijskih tehnologija
*
* -----------------------------------------------------
* Ispitni zadatak iz predmeta:
*
* DIGITALNA VIDEOTEHNIKA
* -----------------------------------------------------
* TV aplikacija
* -----------------------------------------------------
*
* \file remote.h
* \brief
* Ovaj modul se koristi za rad daljinksog upravljača
* Kreirano on Jun 3
*
* @Author Jakob Triva
* \notes
*****************************************************************************/
#include <linux/input.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define MAX_VOLUME	      100
#define NO_ERROR 		      0
#define ERROR			        1
#define NUM_EVENTS        5

typedef struct
{
  char deviceName[20];
  struct input_event* eventBuf;
  uint32_t eventCnt;
  int32_t inputFileDesc;
  uint8_t powerOff;
  uint16_t nOfChannels;
  uint16_t currentCh;
  uint16_t dialedCh;
  uint16_t currentVol;
  uint8_t mute;
  uint8_t volumeChanged;
  uint8_t channelChanged;
  uint8_t menuFlag;
  uint8_t menuChFlag;
  uint16_t menuCh;
  uint8_t infoFlag;
  pthread_t remoteThreadId;
}remoteParameters;

int32_t RM_getKeys(int32_t count, remoteParameters *remoteParams);
/***************************************************************************
*
* @brief Thread funkcija.
*
* @param args- remoteParameters struktura
*
****************************************************************************/
void *RM_readRemote(void *args);
/***************************************************************************
*
* @brief Inicijalizacija parametara.
*
* @param remoteParams- struktura u koje ce se zapisati inicijalizacijski parametri, pat - parsirana PAT tablica, pmts - parsirane PMT tablice
*
****************************************************************************/
int RM_Init(remoteParameters *remoteParams, PAT_table *pat, PMT_table *pmts);
/***************************************************************************
*
* @brief Deinicijalizacija parametara.
*
* @param remoteParams- struktura sa potrebnim vrijednostima
*
****************************************************************************/
void RM_Denit(remoteParameters *remoteParams);
