/****************************************************************************
*
* Sveučilište Josipa Jurja Strossmayera u Osijeku
* Fakultet elektrotehnike, računarstva i informacijskih tehnologija
*
* -----------------------------------------------------
* Ispitni zadatak iz predmeta:
*
* DIGITALNA VIDEOTEHNIKA
* -----------------------------------------------------
* TV aplikacija
* -----------------------------------------------------
*
* \file demux.c
* \brief
* Ovaj modul se koristi za rad demultipleksera, parsiranje PSI i SI tablica
* Kreirano on Jun 3
*
* @Author Jakob Triva
* \notes
*****************************************************************************/
#include "demux.h"

#define RUNNING       4
#define NOT_RUNNING   1
#define EITS          4

pthread_cond_t statusCondition= PTHREAD_COND_INITIALIZER;
pthread_mutex_t statusMutex= PTHREAD_MUTEX_INITIALIZER;;

uint8_t EITfilled()
{
  int i=0;
  for(i=0;i<EITS;i++)
  {
    if(eits[i].currentFlag==0 || eits[i].nextFlag==0)
    {
        return 0;
    }
  }
  return 1;
}

int32_t DM_findChannelIndex(int programNumber){
  int i=0;
  for(i=1;i<pat.nOfChannels;i++){
    if(pmts[i-1].programNumber==programNumber){
        return i-1;
    }
  }
  return -1;
}

int32_t myPrivateTunerStatusCallback(t_LockStatus status)
{
    if(status == STATUS_LOCKED)
    {
        pthread_mutex_lock(&statusMutex);
        pthread_cond_signal(&statusCondition);
        pthread_mutex_unlock(&statusMutex);
        ("\n\n\tCALLBACK LOCKED\n\n");
    }
    else
    {
        printf("\n\n\tCALLBACK NOT LOCKED\n\n");
    }
    return 0;
}

int32_t myEitFilterCallback(uint8_t *buffer)
{
  if(!EITfilled())
  {
    uint16_t channelIndex;
    uint8_t tableId=buffer[0];
    uint16_t sectionLength = (((uint16_t)buffer[1] & 0b00001111) << 8)+ (uint16_t)buffer[2];
    uint16_t programNumber = (((uint16_t)buffer[3]) << 8)+ (uint16_t)buffer[4];
    uint16_t streamId = (((uint16_t)buffer[8]) << 8)+ (uint16_t)buffer[9];
    uint16_t networkId = (((uint16_t)buffer[10]) << 8)+ (uint16_t)buffer[11];

    uint8_t current_next = (uint8_t)buffer[5] & 0b00000001;
    channelIndex = DM_findChannelIndex(programNumber);
    uint16_t memoryUsed=0;
    uint16_t eventIdIndex = 14;
    uint16_t descriptor=0;
    uint16_t nOfDescriptors=0;
    int i, j;
    char *text;
    char* start;
    while(memoryUsed<(sectionLength-15))
    {
      descriptor=(uint16_t)buffer[eventIdIndex+11]+(((uint16_t)buffer[eventIdIndex+10] & 0b00001111)<<8);
      eventIdIndex=eventIdIndex+12+descriptor;
      memoryUsed += (12+descriptor);
      nOfDescriptors++;
    }

    eventIdIndex = 14;
    uint8_t running_status;

    for(i=0;i<nOfDescriptors;i++)
    {
      uint8_t row=0;
      uint16_t length=0;
      uint16_t event_id= ((uint16_t)buffer[eventIdIndex]<<8)+(uint16_t)buffer[eventIdIndex+1];
      running_status = ((uint8_t)buffer[eventIdIndex+10]&0b11100000)>>5;
      descriptor=(uint16_t)buffer[eventIdIndex+11]+(((uint16_t)buffer[eventIdIndex+10] & 0b00001111)<<8);
      uint16_t k=0;
      memoryUsed=0;
      text=(char*)malloc((descriptor+descriptor/80)*sizeof(char));
      for(j=0;j<descriptor;j++)
      {
        if(j%80==0 && row==2)
        {
          text[j+k]='\n';
          k++;
          length+=1;
        }
        text[j+k]=(char)buffer[eventIdIndex+12+j];
        if(text[j+k]=='\0' || text[j+k]=='\n')
        {
          if(row==1)
          {
            start=text+j+k+13;
          }
          row+=1;
          if(row==3)
          {
            if(running_status==NOT_RUNNING)
            {
              eits[channelIndex].nextDescription=(char*)malloc((length+1)*sizeof(char));
              strncpy(eits[channelIndex].nextDescription, start, length);
              eits[channelIndex].nextFlag = 1;
            }
            else if(running_status==RUNNING)
            {
              eits[channelIndex].currentDescription=(char*)malloc((length+1)*sizeof(char));
              strncpy(eits[channelIndex].currentDescription, start, length);
              eits[channelIndex].currentFlag = 1;
            }
          }
        }
        if(row==2)
        {
          length+=1;
        }
      }
      eventIdIndex=eventIdIndex+12+descriptor;
      free(text);
    }
  }
}

int32_t myPatFilterCallback(uint8_t *buffer)
{
	int tableId=buffer[0];
	int i;
    if(!pat.PATflag)
    {
        pat.tableId=buffer[0];
        pat.sectionSyntaxIndicator = buffer[1] & 0b00000001;
        pat.sectionLength= (uint16_t)buffer[2] + (((uint16_t)buffer[1] & 0b00001111) << 8);

	    uint8_t n=(pat.sectionLength-9)/4;

        if(pat.nOfChannels!=n)
        {
            pat.nOfChannels=n;
            free(pat.programNumbers);
            free(pat.mapPids);
            pat.programNumbers=(uint16_t*) malloc(n*sizeof(uint16_t));
            pat.mapPids=(uint16_t*) malloc((n)*sizeof(uint16_t));
        }
	    for(i=0;i<n;i++)
      {
		    pat.programNumbers[i]=(uint16_t)buffer[9+i*4]+(((uint16_t)buffer[8+i*4])<<8);
		    pat.mapPids[i]=((((uint16_t)buffer[10+i*4])<<8)+(uint16_t)buffer[11+i*4]) & 0b0001111111111111;
		    //printf("\npid= %d\n", pat.mapPids[i]);
	    }
        pat.PATflag=1;
    }

}
int32_t myPmtFilterCallback(uint8_t *buffer)
{
	int i;
	if(!pmt.PMTflag)
  {
    pmt.tableId = buffer[0];
    pmt.sectionLength = (uint16_t)buffer[2] + (((uint16_t)buffer[1] & 0b00001111) << 8);
    pmt.programInfoLength = (uint16_t)buffer[11] + (((uint16_t)buffer[10]& 0b00001111) << 8);
    pmt.programNumber = (uint16_t)buffer[4] + ((uint16_t)buffer[3] << 8);
    uint16_t memoryUsed=0;
    uint16_t streamTypeIndex = 12 + pmt.programInfoLength;
    uint16_t descriptor=0;
    uint16_t nOfStreams=0;
    while(memoryUsed<pmt.sectionLength-13)
    {
      descriptor=(uint16_t)buffer[streamTypeIndex+4]+(((uint16_t)buffer[streamTypeIndex+3] & 0b00001111)<<8);
      streamTypeIndex=streamTypeIndex+5+descriptor;
      memoryUsed += (5+descriptor);
      nOfStreams++;
    }
    pmt.n = nOfStreams;
    pmt.streamTypes=(uint8_t*) malloc(nOfStreams*sizeof(uint8_t));
    pmt.elemPids=(uint16_t*) malloc(nOfStreams*sizeof(uint16_t));
    streamTypeIndex=12 + pmt.programInfoLength;
    descriptor=0;

    for(i=0;i<nOfStreams;i++)
    {
      descriptor=(uint8_t)buffer[streamTypeIndex+4]+(((uint8_t)buffer[streamTypeIndex+3] & 0b00001111)<<8);
      pmt.streamTypes[i]=(uint8_t)buffer[streamTypeIndex];
      pmt.elemPids[i]=(uint16_t)buffer[streamTypeIndex+2] +(((uint16_t)buffer[streamTypeIndex+1] & 0b00011111)<<8);
      //printf("stream type= %d, elem_pid= %d \n \n", pmt.streamTypes[i], pmt.elemPids[i]);
      streamTypeIndex=streamTypeIndex+5+descriptor;
    }
    pmt.PMTflag=1;
  }
  return 0;
}


void DM_Init(demuxParameters *demuxParams, configuration *cfg)
{
  demuxParams->playerHandle = 0;
  demuxParams->sourceHandle = 0;
  demuxParams->filterHandle = 0;
  demuxParams->videoHandle = 0;
  demuxParams->audioHandle = 0;
  gettimeofday(&demuxParams->now,NULL);
  demuxParams->lockStatusWaitTime.tv_sec = demuxParams->now.tv_sec+10;

  demuxParams->result = Tuner_Init();
  ASSERT_TDP_RESULT(demuxParams->result, "Tuner_Init");

  demuxParams->result = Tuner_Register_Status_Callback(myPrivateTunerStatusCallback);
  ASSERT_TDP_RESULT(demuxParams->result, "Tuner_Register_Status_Callback");

  demuxParams->result = Tuner_Lock_To_Frequency(cfg->frequency, cfg->bandwidth, cfg->module);
  ASSERT_TDP_RESULT(demuxParams->result, "Tuner_Lock_To_Frequency");

  pthread_mutex_lock(&statusMutex);
  if(ETIMEDOUT == pthread_cond_timedwait(&statusCondition, &statusMutex, &demuxParams->lockStatusWaitTime))
  {
      printf("\n\nLock timeout exceeded!\n\n");
      return -1;
  }
  pthread_mutex_unlock(&statusMutex);
  demuxParams->result = Player_Init(&demuxParams->playerHandle);
  ASSERT_TDP_RESULT(demuxParams->result, "Player_Init");

  demuxParams->result = Player_Source_Open(demuxParams->playerHandle, &demuxParams->sourceHandle);
  ASSERT_TDP_RESULT(demuxParams->result, "Player_Source_Open");

  pat.PATflag = 0;
  pmt.PMTflag = 0;

  //TO DO: count streams
  eits=(EIT_table*)malloc(EITS*sizeof(EIT_table));
  int i;
  for(i=0;i<EITS;i++)
  {
    eits[i].currentFlag=0;
    eits[i].nextFlag=0;
  }
}

void DM_Deinit(demuxParameters *demuxParams)
{
  /* Close previously opened source */
  demuxParams->result = Player_Source_Close(demuxParams->playerHandle, demuxParams->sourceHandle);
  ASSERT_TDP_RESULT(demuxParams->result, "Player_Source_Close");

  /* Deinit player */
  demuxParams->result = Player_Deinit(demuxParams->playerHandle);
  ASSERT_TDP_RESULT(demuxParams->result, "Player_Deinit");

  /* Deinit tuner */
  demuxParams->result = Tuner_Deinit();
  ASSERT_TDP_RESULT(demuxParams->result, "Tuner_Deinit");

  free(eits);
  free(pmts);
}

void DM_ParsePat(demuxParameters *demuxParams)
{
  demuxParams->result = Demux_Set_Filter(demuxParams->playerHandle, 0x0000, 0x00, &demuxParams->filterHandle);
  ASSERT_TDP_RESULT(demuxParams->result, "Demux_Set_Filter");

  demuxParams->result = Demux_Register_Section_Filter_Callback(myPatFilterCallback);
  ASSERT_TDP_RESULT(demuxParams->result, "Demux_Register_Section_Filter_Callback");

  fflush(stdin);
  while(!pat.PATflag)
  {

  }

  demuxParams->result = Demux_Unregister_Section_Filter_Callback(myPatFilterCallback);
  ASSERT_TDP_RESULT(demuxParams->result, "Demux_Unegister_Section_Filter_Callback");

  demuxParams->result = Demux_Free_Filter(demuxParams->playerHandle, demuxParams->filterHandle);
  ASSERT_TDP_RESULT(demuxParams->result, "Demux_Free_Filter");
}

void DM_ParsePmt(demuxParameters *demuxParams)
{
  pmts=(PMT_table*)malloc((pat.nOfChannels-1)*sizeof(PMT_table));
  int i=0;
  for (i=1;i<pat.nOfChannels;i++)
  {
    demuxParams->result = Demux_Set_Filter(demuxParams->playerHandle, pat.mapPids[i], 0x02, &demuxParams->filterHandle);
    ASSERT_TDP_RESULT(demuxParams->result, "Demux_Set_Filter");

    pmt.PMTflag=0;
    demuxParams->result = Demux_Register_Section_Filter_Callback(myPmtFilterCallback);
    ASSERT_TDP_RESULT(demuxParams->result, "Demux_Register_Section_Filter_Callback");

    fflush(stdin);

    while(!pmt.PMTflag)
    {
    }
    pmts[i-1]=pmt;
    demuxParams->result = Demux_Unregister_Section_Filter_Callback(myPmtFilterCallback);
    ASSERT_TDP_RESULT(demuxParams->result, "Demux_Unegister_Section_Filter_Callback");

    demuxParams->result = Demux_Free_Filter(demuxParams->playerHandle, demuxParams->filterHandle);
    ASSERT_TDP_RESULT(demuxParams->result, "Demux_Free_Filter");
  }
}


void DM_ParseEit(demuxParameters *demuxParams)
{
  demuxParams->result = Demux_Set_Filter(demuxParams->playerHandle, 0x0012, 0x4E, &demuxParams->filterHandle);
  ASSERT_TDP_RESULT(demuxParams->result, "Demux_Set_Filter");

  demuxParams->result = Demux_Register_Section_Filter_Callback(myEitFilterCallback);
  ASSERT_TDP_RESULT(demuxParams->result, "Demux_Register_Section_Filter_Callback");

  fflush(stdin);

  while(!EITfilled())
  {
  }

  demuxParams->result = Demux_Unregister_Section_Filter_Callback(myEitFilterCallback);
  ASSERT_TDP_RESULT(demuxParams->result, "Demux_Unegister_Section_Filter_Callback");

  demuxParams->result = Demux_Free_Filter(demuxParams->playerHandle, demuxParams->filterHandle);
  ASSERT_TDP_RESULT(demuxParams->result, "Demux_Free_Filter");
}

uint16_t PMT_FindApid(PMT_table *str)
{
  int i=0;
  for(i=0;i<str->n;i++)
  {
    if(str->streamTypes[i]==3)
    {
      return str->elemPids[i];
    }
  }
  return 0;
}
uint16_t PMT_FindVpid(PMT_table *str)
{
  int i=0;
  for(i=0;i<str->n;i++)
  {
    if(str->streamTypes[i]==2)
    {
      return str->elemPids[i];
    }
  }
  return 0;
}

uint8_t PMT_FindSubtitles(PMT_table *str)
{
  int i=0;
  for(i=0;i<str->n;i++)
  {
    if(str->streamTypes[i]==6)
    {
      return 1;
    }
  }
  return 0;
}