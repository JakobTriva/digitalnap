/****************************************************************************
*
* Sveučilište Josipa Jurja Strossmayera u Osijeku
* Fakultet elektrotehnike, računarstva i informacijskih tehnologija
*
* -----------------------------------------------------
* Ispitni zadatak iz predmeta:
*
* DIGITALNA VIDEOTEHNIKA
* -----------------------------------------------------
* TV aplikacija
* -----------------------------------------------------
*
* \file demux.h
* \brief
* Ovaj modul se koristi za rad demultipleksera, parsiranje PSI i SI tablica
* Kreirano on Jun 3
*
* @Author Jakob Triva
* \notes
*****************************************************************************/
#include "tdp_api.h"
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "ini.h"
#include <errno.h>
#include <linux/input.h>
#include <string.h>

#define ASSERT_TDP_RESULT(x,y)  if(NO_ERROR == x)               \
                                    printf("%s success\n", y);  \
                                else{                           \
                                    textColor(1,1,0);           \
                                    printf("%s fail\n", y);     \
                                    textColor(0,7,0);           \
                                    return -1;                  \
                                }

static inline void textColor(int32_t attr, int32_t fg, int32_t bg)
{
   char command[13];
   sprintf(command, "%c[%d;%d;%dm", 0x1B, attr, fg + 30, bg + 40);
   printf("%s", command);
}

typedef struct
{
  struct timespec lockStatusWaitTime;
  struct timeval now;
  uint32_t playerHandle;
  uint32_t sourceHandle;
  uint32_t filterHandle;
  uint32_t videoHandle;
  uint32_t audioHandle;
  int32_t result;
  uint16_t menuCh;
}demuxParameters;

typedef struct
{
  uint16_t *programNumbers;
  uint16_t *mapPids;
  uint8_t tableId;
  uint8_t	sectionSyntaxIndicator;
  uint16_t sectionLength;
  uint8_t nOfChannels;
  uint8_t PATflag;
}PAT_table;

typedef struct
{
  uint8_t *streamTypes;
  uint16_t *elemPids;
  uint8_t tableId;
  uint8_t PMTflag;
  uint16_t sectionLength;
  uint16_t programInfoLength;
  uint16_t programNumber;
  int n;
}PMT_table;

typedef struct
{
  char *currentDescription;
  char *nextDescription;
  uint8_t nextFlag;
  uint8_t currentFlag;
}EIT_table;

PMT_table *pmts;
PMT_table pmt;
PAT_table pat;
EIT_table *eits;

/***************************************************************************
*
* @brief Inicijalizacija parametara.
*
* @param demuxParams- struktura u koje ce se zapisati inicijalizacijski parametri, cfg- struktura sa podatcima iz konfiguracijske datoteke
*
****************************************************************************/
void DM_Init(demuxParameters *demuxParams, configuration *cfg);
/***************************************************************************
*
* @brief Deinicijalizacija parametara.
*
* @param demuxParams- struktura sa potrebnim vrijednostima
*
****************************************************************************/
void DM_Deinit(demuxParameters *demuxParams);
/***************************************************************************
*
* @brief Funkcija koja vraca Vpid kanala
*
* @param str - Struktura sa podacima
* @return Vpid, ako ima
* 0, u slucaju da ne postoji Vpid
*
****************************************************************************/
uint8_t PMT_FindVpid(PMT_table *str);
/***************************************************************************
*
* @brief Opis funkcije.
*
* @param str - Struktura sa podacima
* @return Apid, ako ima
* 0, u slucaju da ne postoji Apid
*
****************************************************************************/
uint8_t PMT_FindApid(PMT_table *str);
/***************************************************************************
*
* @brief Funkcija koja vraca Apid kanala
*
* @param str - Struktura sa podacima
*
* @return 1, ako postoje podnaslovi
* 0, u slucaju da ne postoje podnaslovi
*
****************************************************************************/
uint8_t PMT_FindSubtitles(PMT_table *str);
/***************************************************************************
*
* @brief Parsiranje PMT tablice.
*
* @param demuxParams- struktura sa potrebnim vrijednostima
*
****************************************************************************/
void DM_ParsePmt(demuxParameters *demuxParams);
/***************************************************************************
*
* @brief Parsiranje PAT tablice.
*
* @param demuxParams- struktura sa potrebnim vrijednostima
*
****************************************************************************/
void DM_ParsePat(demuxParameters *demuxParams);
/***************************************************************************
*
* @brief Parsiranje EIT tablice.
*
* @param demuxParams- struktura sa potrebnim vrijednostima
*
****************************************************************************/
void DM_ParseEit(demuxParameters *demuxParams);
