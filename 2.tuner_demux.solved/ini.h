/****************************************************************************
*
* Sveučilište Josipa Jurja Strossmayera u Osijeku
* Fakultet elektrotehnike, računarstva i informacijskih tehnologija
*
* -----------------------------------------------------
* Ispitni zadatak iz predmeta:
*
* DIGITALNA VIDEOTEHNIKA
* -----------------------------------------------------
* TV aplikacija
* -----------------------------------------------------
*
* \file ini.h
* \brief
* Ovaj modul se koristi za parsiranje konfiguracijske datoteke
* Kreirano on Jun 3
*
* @Author Jakob Triva
* \notes
*****************************************************************************/
#include <stdio.h>

typedef struct
{
    int frequency;
    int module;
    int bandwidth;
    int apid;
    int atype;
    int vpid;
    int vtype;
}configuration;

/***************************************************************************
*
* @brief Čitanje konfiguracijske datoteke
*
* @param cfg- struktura u koje ce se zapisati inicijalizacijski parametri, fileName - put do konfiguracijske datoteke
*
****************************************************************************/
int CFG_ReadConfig(configuration *cfg, char* fileName);
/***************************************************************************
*
* @brief Konzolno ispisivanje pročitanih parametara
*
* @param cfg- struktura u koje su zapisani inicijalizacijski parametri
*
****************************************************************************/
void CFG_PrintConfig(configuration *cfg);
